<div class="main">
    <div id="login-form">

        <div class="login-container">
            <figure class="logo">
                <img src="http://localhost/medicamento/assets/images/logo_procaps.png" alt="Logo" width="30%">
            </figure>
            
            <h3 class="title text-center">Iniciar sesión</h3>

            
            <div class="alert-danger" id="errors">
                <ul class="items-error">
                    <?php foreach ($errors as $key => $value) { ?>
                        <li> <?php echo $value ?> </li>
                    <?php } ?>
                </ul>
            </div>

            <!-- <?php echo form_open('Auth/UserController/sign_in'); ?> -->
            <form id="loginForm">
                <div class="form-group">
                    <label for="username">Nombre de usuario</label>
                    <input  type="text" 
                            class="form-control animated" 
                            name="username"
                            placeholder="Ingresa tu nombre de usuario"
                            id="username" value="<?php echo ($usuario) ?: '' ?>"
                            required/>
                </div>
                
                <div class="form-group">
                    
                    <label for="password">Contraseña</label>
                    <input  type="password"
                            class="form-control animated"
                            id="password"
                            name="password"
                            placeholder="Ingresa tu nombre de contraseña"
                            value=""
                            required/>
                </div>
                
                <div class="form-group">
                    <label for="recuerdame" id="rem">
                        <input type="checkbox" name="remember" id="recuerdame">
                        Recordar mis datos 
                    </label>
                </div>

                <div class="form-group">
                    <button type="submit" class="button animated block button-blue"> Ingresar </button>
                </div>
            </form>

            <a href="signUp">Registrate </a>
            <p class="footer" style="font-style: italic;">Page rendered in <strong>{elapsed_time}</strong> seconds. <br> <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
        </div>
        
    </div>
    <div class="display-bg loading" id="display-bg"></div>
</div>