<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

    public static $msgErrors;
    public static $now;
    const MAX_ATTEMPTS = 5;

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['encryption','form_validation', 'session']);
        $this->load->helper(['url', 'cookie']);
        $this->load->model('UserModel');
        self::$msgErrors = [
            'required'   => 'El campo %s es obligatorio',
            'max_length' => 'Demasiados caracteres para %s',
            'min_length' => 'Faltan caracteres en %s',
            'matches'    => 'El campo %s no coincide con %s'
        ];
        self::$now      = date('Y-m-d H:i:s');
    }


    /**
     * signIn
     * retorna la vista para iniciar sesión
    */
    public function signIn()
	{
		$errors = [];
        $user  	= null;
		if ($this->session->has_userdata('errors')) {
			$errors = $this->session->errors;
			$this->session->unset_userdata('errors');
		}
		if ($this->session->has_userdata('user')) {
            $user = $this->encryption->decrypt($this->session->user);
            $this->session->unset_userdata('user');
        }

        $scripts = [
            '<script src="node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>',
            '<script src="assets/js/auth/login.js"></script>'
        ];
        $styles = [
            '<link rel="stylesheet" href="assets/css/auth/login.css"/>'
        ];

		$data = [
			'view'    => 'signin',
			'errors'  => $errors,
            'usuario' => $user,
            'title'   => 'Iniciar sesión',
            'scripts' => $scripts,
            'styles'  => $styles
		];
		$this->load->view('auth/global', $data);
    }

    /**
     * sign_in
     * valida los datos recibidos del cliente
     * para iniciar sesión
    */
    public function sign_in()
    {
        $post = $this->input->post();
        $errors = [
            'status' => false,
            'data'   => []
        ];
        $validations = [
            [
                'field'  => 'username',
                'label'  => 'Nombre de usuario',
                'rules'  => 'required|min_length[4]|max_length[18]',
                'errors' => self::$msgErrors
            ],
            [
                'field'  => 'password',
                'label'  => 'Contraseña',
                'rules'  => 'required|min_length[8]|max_length[20]',
                'errors' => self::$msgErrors
            ]
        ];
        $this->form_validation->set_rules($validations);
        $result = $this->form_validation->run();
        if (!$result) 
        {
            $errors['data'] = $this->form_validation->error_array();
            $this->saveErrors($errors);
        }
        $fieldsUser = [
            'username', 
            'status', 
            'password', 
            'user_id', 
            'firstname', 
            'lastname'
        ];
        $search = $this->UserModel->findUser($post['username'], $fieldsUser);
        if ( count($search) == 0 ) 
        {
            $errors['data'] = (object)['Este usuario no existe'];
            $this->saveErrors($errors);
        } 
        else 
        {
            $url    = 'signIn';
            if($search[0]->status == 'inactive')
            {
                $errors['data'] = (object)['Este usuario se encuentra inactivo'];
            } 
            elseif ( $search[0]->status == 'block' ) 
            {
                $errors['data'] = (object)['Este usuario se encuentra bloqueado'];
            } 
            elseif ( $search[0]->status == 'active' ) 
            {
                $valid = $this->verifyPassword($post['password'], $search[0]->password);
                if ($valid) 
                {
                    if ($this->clearAttempts($search[0]->user_id)){
                        $_token = $this->generateToken($search[0]->user_id);
                        $errors['status'] = true;
                        $errors['url']    = 'Dashboard/Home';
                        $errors['data']   = (object)['logueado correctamente'];
                        $datos = json_encode([
                            'user'      => $search[0]->user_id,
                            'status'    => $search[0]->status,
                            'full_name' => "{$search[0]->firstname} {$search[0]->lastname}",
                            '_token'    => $_token
                        ]);
                        $this->session->set_userdata('data_user', $this->encryption->encrypt($datos));
                        $this->session->mark_as_temp('data_user', 86400);
                        
                        if (isset($post['remember']) && $post['remember'] == 'on') {
                            $errors['_token'] = json_encode([
                                'usr' => $this->generateToken($post['username']), 
                                '_p'  => $this->generateToken($post['password'])
                            ]);
                        }

                    } else {
                        $errors = ['Ha ocurrido un error inesperado #$3'];  
                    }
                }
                else
                {
                    $errors['data'] = (object)['El usuario y la contraseña no coinciden'];
                    $this->verifyAttempts($search[0]->user_id, $errors);
                }
            } 
            else 
            {
                $errors['data'] = (object)['Ha ocurrido un error inesperado'];
            }
            $this->saveErrors($errors);
        }
    }
    

    /**
     * signUp
     * retorna la vista para crear nuevo usuario
    */
	public function signUp()
	{
		$errors = [];
		if ($this->session->has_userdata('errors')) {
            $errors = $this->session->errors;
        }
		$data = [
			'view' 	 => 'signup',
			'errors' => $errors,
            'title'   => 'Crear usuario'
		];
		$this->load->view('auth/global', $data);
    }
    
    /**
     * sign_up
     * valida los datos recibidos del cliente
     * para crear un nuevo usuario
    */
	public function sign_up()
	{
        $post = $this->input->post();
        $validations = [
            [
                'field'  => 'username',
                'label'  => 'Nombre de usuario',
                'rules'  => 'required|min_length[4]|max_length[18]',
                'errors' => self::$msgErrors
            ],
            [
                'field'  => 'firstname',
                'label'  => 'Nombres',
                'rules'  => 'required|min_length[5]|max_length[20]',
                'errors' => self::$msgErrors
            ],
            [
                'field'  => 'lastname',
                'label'  => 'Apellidos',
                'rules'  => 'required|min_length[5]|max_length[20]',
                'errors' => self::$msgErrors
            ],
            [
                'field'  => 'password',
                'label'  => 'Contraseña',
                'rules'  => 'required|min_length[8]|max_length[20]',
                'errors' => self::$msgErrors
            ],
            [
                'field'  => 'passconf',
                'label'  => 'Confirmacion Contraseña',
                'rules'  => 'required|min_length[8]|max_length[20]|matches[password]',
                'errors' => self::$msgErrors
            ],
            [
                'field'  => 'email',
                'label'  => 'Correo electronico ',
                'rules'  => 'required|valid_email',
                'errors' => self::$msgErrors
            ],
        ];

        $this->form_validation->set_rules($validations);
        
        $result = $this->form_validation->run();
        if (!$result) {
            $errors = $this->form_validation->error_array();
            $errors['status'] = false;
            $errors['items']  = $this->form_validation->error_array();
            $this->saveErrors($errors);
        }

        $search = $this->UserModel->findUser($post['username'], ['username']);
        if (count($search) == 0) 
        {
            $user = [
                'username'  => $post['username'],
                'password'  => $this->encodePassword($post['password']),
                'email'     => $this->encryption->encrypt($post['email']),
                'firstname' => $post['firstname'],
                'lastname'  => $post['lastname']
            ];

            $saveUser = $this->UserModel->saveUser($user);

            if ($saveUser) 
            {
                $errors = ['Usuario registrado exitosamente'];
                $this->session->set_userdata('user', $this->encryption->encrypt($post['username']) );
                $this->session->mark_as_temp('user', 300);
                $this->saveErrors($errors, '/signIn');
            }
            else
            {
                $errors = ['No se ha podido guardar el usuario'];
                $this->saveErrors($errors, '/signUp');
            }
        }
        else
        {
            $errors = ['Este usuario ya existe'];
            $this->saveErrors($errors, '/signUp');
        }
    }
    
    /**
     * saveErrors
     * funcion que guarda en sesion los errores para luego
     * ser desplegados en la vista
     * @param Array [errors] arreglo con todos los errores a guardar
     * @param String [redirect] url hacia donde desea redirigir el navegador
    */
    public function saveErrors(Array $errors)
    {
        echo json_encode($errors);
        exit;
    }

    /**
     * encodePassword
     * retorna una contraseña o string enviado por 
     * parametros encriptada
     * @param String [password] cadena a encriptar
     * @return String cadena encriptada por password_hash
    */
    public function encodePassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * verifyPassword
     * retorna la comparacion de dos cadenas si son iguales o no
     * @param String [normalPassword] cadena sin encriptar
     * @param String [passwordEncoded] cadena encriptada a comparar
     * @return Boolean true cuando las cadenas coinciden, false si no coinciden
    */
    public function verifyPassword($normalPassword, $passwordEncoded)
    {
        return password_verify($normalPassword, $passwordEncoded);
    }

    /** 
     * signOut
     * Funcion para borrar los datos del usuario y de esta forma
     * desloguearse
    */
    public function signOut()
    {
        $this->session->unset_userdata(['data_user', 'errors']);
        redirect('/');
    }


    /** 
     * verifyAttempts
     * Funcion para insertar o guardar los intentos fallidos en caso de
     * equivocarse al ingresar
     * @param Int [userId] id del usuario
     * @param Array [&errors] array de errores enviados por referencia para que sean alterados
    */
    public function verifyAttempts(int $userId, Array &$errors)
    {
        $counts_attempts = $this->UserModel->getAttemptsUser($userId);
        $saveAttempts = [
            'user_id'       => $userId,
            'last_login'    => self::$now,
            'last_login_ip' => $this->input->ip_address(),
        ];
        if (count($counts_attempts) == 0) {
            $saveAttempts['attempts'] = 1;
            $this->UserModel->insertAttempt($saveAttempts);
        } else {
            $saveAttempts['attempts'] = ($counts_attempts[0]->attempts >= 5) ? 5 : $counts_attempts[0]->attempts + 1;
            $this->UserModel->updateAttempt($saveAttempts);
        }
        $attempts = self::MAX_ATTEMPTS - $saveAttempts['attempts'];
        if($attempts <= 0){
            $updateDataUser = [
                'status'  => 'block',
                'user_id' => $userId
            ];
            $this->UserModel->updateStatus($updateDataUser);
            $errors[0] = "Usuario bloqueado";
        } else {
            $errors[] = "Quedan {$attempts} intentos";
        }
    }

    /** 
     * clearAttempts
     * Funcion para insertar o guardar los intentos fallidos en 0
     * @param Int [userId] id del usuario
     * @return Boolean [true|false] false si ocurrio un error en la consulta
    */
    public function clearAttempts($userId) {
        $counts_attempts = $this->UserModel->getAttemptsUser($userId);
        $saveAttempts = [
            'user_id'       => $userId,
            'last_login'    => self::$now,
            'last_login_ip' => $this->input->ip_address(),
            'attempts'      => 0
        ];
        $result = false;
        if (count($counts_attempts) == 0)
        {
            $result = $this->UserModel->insertAttempt($saveAttempts);
        }
        else
        {
            $result = $this->UserModel->updateAttempt($saveAttempts);
        }

        return $result;
    }

    public function generateToken(String $data)
    {
        $string = base64_encode($data);
        $key    = json_encode(['_key' => $string]);
        return  $this->encryption->encrypt($key);
    }

    public function decodeToken(String $_token)
    {
        $string  = $this->encryption->decrypt($_token);
        $key     = json_decode($string, true);
        $_tokenD = base64_decode($key['_key']); 
        return  $_tokenD;
    }


    public function Remember()
    {
        foreach ($this->input->post() as $key => $value) {
            $data[$key] = $this->decodeToken($value);
        }
        echo json_encode($data);
    }

}
