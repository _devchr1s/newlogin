<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public static $userData;
	public function __construct()
    {
        parent::__construct();
        $this->load->library(['encryption','form_validation', 'session']);
        $this->load->helper(['url', 'cookie']);
		if (!$this->checkLogin()) {
			$this->session->unset_userdata('data_user');
			$this->session->set_userdata('errors', ['Su sesión ha caducado.']);
			redirect('signIn');
		}
		self::$userData = json_decode(
			$this->encryption->decrypt($this->session->data_user), 
			TRUE
		);
	}

	public function index(){
		$this->session->unset_userdata('errors');
		redirect('Dashboard/Home');
	}
	
	
	/**	
	 * Home
	 * metodo que despliega la vista principal
	*/
	public function Home()
	{
		$data = [
			'view'    	=> 'home',
			'title'		=> 'Home',
			'fullname' 	=> self::$userData['full_name']
		];
		$this->load->view('dashboard/global', $data);
	}

	/**	
	 * checkLogin
	 * funcion para verificar si el usuario está logueado
	*/
	public function checkLogin()
	{
		if (!$this->session->has_userdata('data_user')) {
			return false;
		} else {
			try
			{
				$key = $this->encryption->decrypt($this->session->data_user);
				if (! $data = json_decode($key, true) ) return false;
				$userId = $data['user'];
				$token  = $data['_token'];
				if (! $_tokenId = $this->decodeToken($token) ) return false;
				if ( $_tokenId != $userId ) return false;
			}
			catch (Exception $e)
			{
				return false;
			}
		}
		return true;
	}


	public function decodeToken(String $token)
	{
/* 		$string = base64_encode($userId);
        $key    = json_encode(['_key' => $string]);
		return  $this->encryption->encode($key); */
		try
		{
			$key = $this->encryption->decrypt($token);
			if (!$data = json_decode($key, TRUE)) return false;
			$data = $data['_key'];
			if (!$tokenId = base64_decode($data)) return false;
			return $tokenId;
		}
		catch (Exception $e)
		{
			echo "fail token #1";
			return false;
		}
	}
}
