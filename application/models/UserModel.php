<?php
class UserModel extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->db->set_dbprefix('prefix_');
    }


    /**
     * findUser
     * Filtra los registros por nombre de usuario,
     * retorna la existencia de un registro en la tabla
     * 'users'
     * @param String [username] nombre del usuario
     * @param Array [fields] los campos que desea visualizar
     * @return [Array || Exception]
    */
    public function findUser(String $username, Array $fields)
    {
        try
        {
            $this->db->cache_on();
            $this->db->select($fields);
            $this->db->where('username', $username);
            $data = $this->db->limit(1)->get('users')->result();
            $this->db->cache_off();
            return $data;
        } 
        catch (Exception $e)
        {
            return new Exception('Ha ocurrido un error inesperado');
        }
    }


    /**
     * saveUser
     * guarda un usuario
     * @param Array [user] array con los datos del usuario
     * @return [Boolean || Exception]
    */
    public function saveUser(Array $user)
    {
        try
        {
            $user['status']    = 'active';
            $user['type_user'] = 'comun_user';
            $user['createdAt'] = date('Y-m-d H:i:s');
            return $this->db->insert('users', $user);
        } 
        catch (Exception $e)
        {
            return new Exception('Ha ocurrido guardando el usuario');
        }
    }


    public function getAttemptsUser(Int $userId)
    {
        try
        {
            $this->db->cache_on();
            $this->db->select('attempts');
            $this->db->where('user_id', $userId);
            $data = $this->db->limit(1)->get('users_login_attempts')->result();
            $this->db->cache_off();
            return $data;
        }
        catch (Exception $e)
        {
            return new Exception('Ha ocurrido un error');
        }
    }


    public function insertAttempt(Array $userLoginAttempt){
        try
        {
            return $this->db->insert('users_login_attempts', $userLoginAttempt);
        }
        catch (Exception $e)
        {
            return new Exception('Ha ocurrido guardando el usuario');
        }
    }


    public function updateAttempt(Array $userLoginAttempt){
        try
        {
            $this->db->where('user_id', $userLoginAttempt['user_id']);
            unset($userLoginAttempt['user_id']);
            return $this->db->update('users_login_attempts', $userLoginAttempt);
        }
        catch (Exception $e)
        {
            return new Exception('Ha ocurrido guardando el usuario');
        }
    }


    public function updateStatus(Array $userStatus){
        try
        {
            $this->db->where('user_id', $userStatus['user_id']);
            unset($userStatus['user_id']);
            return $this->db->update('users', $userStatus);
        }
        catch (Exception $e)
        {
            return new Exception('Ha ocurrido guardando el usuario');
        }
    }

}

?>