// Fondo aleatorio
window.onload = changeImageLogin();
function changeImageLogin() {
    // La variable totalImagesLogin, debe ser igual a la cantidad de imagenes a usar
    var totalImagesLogin = 10;
    var randomImageLogin = 1 + Math.floor(Math.random() * totalImagesLogin);
    $('.loading').fadeOut('slow', function () {
        $('.display-bg').css({
            'background': `url(assets/images/login/loginImage_${randomImageLogin}.jpg) no-repeat center center fixed`,
            '-webkit-background-size' : 'cover',
            '-moz-background-size' : 'cover',
            '-o-background-size' : 'cover',
            'background-size' : 'cover'
        }).fadeIn('fast');
    });
    let items = $('#errors ul li').length;
    if (items > 0){
        $('#errors').fadeIn('fast');
    }

    if ($( window ).width() <= 767) {
        $('#login-form').css({
            'background': `url(assets/images/login/loginImage_${randomImageLogin}.jpg) no-repeat center center fixed`,
            '-webkit-background-size' : 'cover',
            '-moz-background-size' : 'cover',
            '-o-background-size' : 'cover',
            'background-size' : 'cover'
        });
    }

}

$(document).ready( () => {
    if (window.localStorage.getItem('remember') != undefined) {
        var remember = JSON.parse(window.localStorage.getItem('remember'));
        $.ajax({
            url: `Auth/UserController/Remember`,
            method: "POST",
            data: remember,
            dataType: "json"
        })
        .done( function (response) {
            $('#username').val(response.usr);
            $('#password').val(response._p);
            $('#recuerdame').attr('checked', true);
            $('#rem').append(`
                <a style="float: right; cursor: pointer;"
                onclick="forgetUser()"> Olvidar </a>
            `)
        });
    }
    $('#loginForm').submit( function (event) {
        event.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            url: `Auth/UserController/sign_in`,
            method: "POST",
            data: data,
            dataType: "json"
        })
        .done( function (response) {
            var msg = '';
            if (!response.status) {
                $.each(response.data, (index, value) => {
                    msg = msg + '<br>' + value;
                });
                swal('Lo sentimos',  msg, 'error');
            } else {
                if ($('#recuerdame').prop('checked')) {
                    window.localStorage.setItem('remember', response._token)
                }
                window.location.href = `${response.url}`;
            }
        })
        .fail(function( jqXHR, textStatus ) {
            swal('Lo sentimos', 'Ha ocurrido un error inesperado, intente nuevamente', 'error');
        });
    });
});


function forgetUser() {
    window.localStorage.removeItem('remember');
    $('#username').val('');
    $('#password').val('');
    $('#rem a').detach();
}